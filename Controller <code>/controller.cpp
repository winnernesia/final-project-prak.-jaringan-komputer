void Update () {
        float speed = 5f;
        Vector3 pos = transform.position;

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            rb2d.AddForce(new Vector2(0, 5), ForceMode2D.Impulse);
            anim.SetTrigger("Jump");
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            pos.y -= speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            pos.x += speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            pos.x -= speed * Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.space))
        {
            anim.SetTrigger("Attack");
        }